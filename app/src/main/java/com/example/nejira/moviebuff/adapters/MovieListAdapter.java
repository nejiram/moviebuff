package com.example.nejira.moviebuff.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.model.Movie;
import com.example.nejira.moviebuff.view.Main;

import java.util.ArrayList;
import java.util.List;

public class MovieListAdapter extends ArrayAdapter<Movie> {

    public int resource;
    public TextView title;
    public TextView releaseDate;
    public ImageView image;
    private String posterPath="https://image.tmdb.org/t/p/w342";

    public MovieListAdapter(@NonNull Main context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View newView;
        if (convertView == null) {
            // ako nije update
            // kreiramo novi objekat i inflate-mo ga
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(inflater);
            layoutInflater.inflate(resource, (ViewGroup) newView, true);
        } else {
            // ako nije update
            // samo mijenjamo vrijednosti polja
            newView = (LinearLayout) convertView;
        }
        // dodajemo reference na view i popunjavamo ga sa vrijednostima polja iz objekta
        Movie movie =  getItem(position);

        title = newView.findViewById(R.id.mTitle);
        releaseDate = newView.findViewById(R.id.mReleaseDate);

        image = newView.findViewById(R.id.icon);

        title.setText(movie.getTitle());
        releaseDate.setText(movie.getReleaseDate());

        Glide.with(getContext())
                .load(posterPath + movie.getPosterPath())
                .centerCrop()
                .placeholder(R.drawable.movie_icon)
                .error(R.drawable.movie_icon)
                .fallback(R.drawable.movie_icon)
                .into(image);

        return newView;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.addAll(movies);
    }

    public Movie getMovie(int position) {
        return this.getItem(position);
    }
}
