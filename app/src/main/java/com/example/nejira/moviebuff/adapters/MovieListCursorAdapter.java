package com.example.nejira.moviebuff.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.model.MovieDBOpenHelper;

public class MovieListCursorAdapter extends ResourceCursorAdapter {

    public TextView title;
    public TextView genre;
    public TextView releaseDate;
    public ImageView image;
    private String posterPath="https://image.tmdb.org/t/p/w342";

    public MovieListCursorAdapter(Context context, int layout, Cursor c, boolean autoRequery) {
        super(context, layout, c, autoRequery);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        title = view.findViewById(R.id.mTitle);
        releaseDate = view.findViewById(R.id.mReleaseDate);
        image = view.findViewById(R.id.icon);

        title.setText(cursor.getString(cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_TITLE)));
        releaseDate.setText(cursor.getString(cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_RELEASEDATE)));

        Glide.with(context)
                .load(posterPath + cursor.getString(cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_POSTERPATH)))
                .centerCrop()
                .placeholder(R.drawable.movie_icon)
                .error(R.drawable.movie_icon)
                .fallback(R.drawable.movie_icon)
                .into(image);

    }
}
