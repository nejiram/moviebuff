package com.example.nejira.moviebuff.model;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class CastContentProvider extends ContentProvider {

    private static final int MOVIE_ID = 1;
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("moviebuff.provider.cast", "elements/#", MOVIE_ID);
    }

    MovieDBOpenHelper movieDBOpenHelper;

    @Override
    public boolean onCreate() {
        movieDBOpenHelper = new MovieDBOpenHelper(getContext(), MovieDBOpenHelper.DATABASE_NAME, null, MovieDBOpenHelper.DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase database;

        try {
            database = movieDBOpenHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            database = movieDBOpenHelper.getReadableDatabase();
        }

        String groupby = null;
        String having = null;
        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)){
            case MOVIE_ID:
                String idRow = uri.getPathSegments().get(1);
                sqLiteQueryBuilder.appendWhere(MovieDBOpenHelper.CAST_MOVIE_ID+"="+idRow);
            default:
                break;
        }

        sqLiteQueryBuilder.setTables(MovieDBOpenHelper.CAST_TABLE);
        Cursor cursor = sqLiteQueryBuilder.query(database, projection, selection, selectionArgs, groupby, having, sortOrder);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            case MOVIE_ID:
                return "vnd.android.cursor.dir/vnd.moviebuff.elemental";
            default:
                throw new IllegalArgumentException("Unsupported uri: " + uri.toString());
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase database;

        try {
            database = movieDBOpenHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            database = movieDBOpenHelper.getReadableDatabase();
        }

        long id = database.insert(MovieDBOpenHelper.CAST_TABLE, null, values);
        return uri.buildUpon().appendPath(String.valueOf(id)).build();

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
