package com.example.nejira.moviebuff.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Movie implements Parcelable {

    private Integer id;
    private Integer internalId;
    private String title;
    private String genre;
    private String releaseDate;
    private String homepage;
    private String overview;
    private String  posterPath;
    private ArrayList<String> cast;
    private ArrayList<String> similarMovies;

    public Movie(Integer id, String title, String genre, String releaseDate, String homepage, String overview, String posterPath) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.overview = overview;
        this.posterPath = posterPath;
        this.cast = new ArrayList<>();
        this.similarMovies = new ArrayList<>();
    }

    public Movie(Integer id, Integer internalId, String title, String genre, String releaseDate, String homepage, String overview, String posterPath) {
        this.id = id;
        this.internalId = internalId;
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.overview = overview;
        this.posterPath = posterPath;
        this.cast = new ArrayList<>();
        this.similarMovies = new ArrayList<>();
    }

    public Movie(Integer id, String title, String genre, String releaseDate, String homepage, String overview, String posterPath, ArrayList<String> cast, ArrayList<String> similarMovies) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.overview = overview;
        this.posterPath = posterPath;
        this.cast = cast;
        this.similarMovies = similarMovies;
    }

    public Movie(Integer id, String title, String releaseDate, String overview, String posterPath) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.overview = overview;
        this.posterPath = posterPath;
        this.cast = new ArrayList<>();
        this.similarMovies = new ArrayList<>();
    }

    protected Movie(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            internalId = null;
        } else {
            internalId = in.readInt();
        }
        title = in.readString();
        genre = in.readString();
        releaseDate = in.readString();
        homepage = in.readString();
        overview = in.readString();
        posterPath = in.readString();
        cast = in.createStringArrayList();
        similarMovies = in.createStringArrayList();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (internalId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(internalId);
        }
        dest.writeString(title);
        dest.writeString(genre);
        dest.writeString(releaseDate);
        dest.writeString(homepage);
        dest.writeString(overview);
        dest.writeString(posterPath);
        dest.writeStringList(cast);
        dest.writeStringList(similarMovies);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public ArrayList<String> getCast() {
        return cast;
    }

    public void setCast(ArrayList<String> cast) {
        this.cast = cast;
    }

    public ArrayList<String> getSimilarMovies() {
        return similarMovies;
    }

    public void setSimilarMovies(ArrayList<String> similarMovies) {
        this.similarMovies = similarMovies;
    }

    public Integer getInternalId() {
        return internalId;
    }

    public void setInternalId(Integer internalId) {
        this.internalId = internalId;
    }

    public static Creator<Movie> getCREATOR() {
        return CREATOR;
    }
}
