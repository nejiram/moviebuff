package com.example.nejira.moviebuff.model;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import com.example.nejira.moviebuff.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MovieDetailInteractor extends IntentService implements iMovieDetailInteractor {

    final public static int STATUS_RUNNING = 0;
    final public static int STATUS_FINISHED = 1;
    final public static int STATUS_ERROR = 2;

    private MovieDBOpenHelper movieDBOpenHelper;
    SQLiteDatabase database;
    private String tmdb_api_key = BuildConfig.API_KEY;
    Movie movie;

    public MovieDetailInteractor() {
        super(null);
    }

    public MovieDetailInteractor(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        // DOHVATANJE DETALJA FILMA SA API-A

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);

        String params = intent.getStringExtra("query");
        String query = null;

        try {
            query = URLEncoder.encode(params, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url1 = "https://api.themoviedb.org/3/movie/" + query + "?api_key=" + tmdb_api_key;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);

            Integer id = jo.getInt("id");
            String title = jo.getString("title");
            String overview = jo.getString("overview");
            String releaseDate = jo.getString("release_date");
            String homepage = jo.getString("homepage");
            String posterPath = jo.getString("poster_path");
            String genre = jo.getJSONArray("genres").getJSONObject(0).getString("name");
            movie = new Movie(id, title, genre, releaseDate, homepage, overview, posterPath);
            AddCast(query);
            AddSimilarMovies(query);

        } catch (ClassCastException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch(MalformedURLException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (IOException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (JSONException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        }

        bundle.putParcelable("result", movie);
        receiver.send(STATUS_FINISHED, bundle);
    }

    protected void AddCast(String... params) {

        // DOHVATANJE ZVIJEZDA FILMA SA API-A

        String query = null;
        try {
            query = URLEncoder.encode(params[0], "utf-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url1 = "https://api.themoviedb.org/3/movie/" + query + "/credits?api_key=" + tmdb_api_key;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);
            JSONArray items = jo.getJSONArray("cast");
            ArrayList<String> list = new ArrayList<>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject star = items.getJSONObject(i);
                String name = star.getString("name");
                list.add(name);
                if (i == 4) break;
            }

            movie.setCast(list);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected void AddSimilarMovies(String... params) {

        // DOHVATANJE SLICNIH FILMOVA SA API-A

        String query = null;
        try {
            query = URLEncoder.encode(params[0], "utf-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://api.themoviedb.org/3/movie/" + query + "/similar?api_key=" + tmdb_api_key;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);
            JSONArray items = jo.getJSONArray("results");
            ArrayList<String> list = new ArrayList<>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject similar = items.getJSONObject(i);
                String title = similar.getString("title");
                list.add(title);
                if (i == 4) break;
            }

            movie.setSimilarMovies(list);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(Movie movie, Context context) {

        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        Uri moviesUri = Uri.parse("content://moviebuff.provider.movies/elements");
        Uri castUri = Uri.parse("content://moviebuff.provider.cast/elements");
        Uri similarUri = Uri.parse("content://moviebuff.provider.similar/elements");

        ContentValues values = new ContentValues();
        values.put(MovieDBOpenHelper.MOVIE_ID, movie.getId());
        values.put(MovieDBOpenHelper.MOVIE_TITLE, movie.getTitle());
        values.put(MovieDBOpenHelper.MOVIE_GENRE, movie.getGenre());
        values.put(MovieDBOpenHelper.MOVIE_HOMEPAGE, movie.getHomepage());
        values.put(MovieDBOpenHelper.MOVIE_OVERVIEW, movie.getOverview());
        values.put(MovieDBOpenHelper.MOVIE_POSTERPATH, movie.getPosterPath());
        values.put(MovieDBOpenHelper.MOVIE_RELEASEDATE, movie.getReleaseDate());
        contentResolver.insert(moviesUri, values);

        for (int i = 0; i < movie.getCast().size(); i++) {
            String name = movie.getCast().get(i);
            ContentValues cast = new ContentValues();
            cast.put(MovieDBOpenHelper.CAST_NAME, name);
            cast.put(MovieDBOpenHelper.CAST_MOVIE_ID, movie.getId());
            contentResolver.insert(castUri, cast);
        }

        for (int i = 0; i < movie.getSimilarMovies().size(); i++) {
            String title = movie.getSimilarMovies().get(i);
            ContentValues similarMovies = new ContentValues();
            similarMovies.put(MovieDBOpenHelper.SMOVIE_TITLE, title);
            similarMovies.put(MovieDBOpenHelper.SMOVIES_MOVIE_ID, movie.getId());
            contentResolver.insert(similarUri, similarMovies);
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    public Movie getMovie(Context context, Integer id) {

        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        String[] columns = null;
        Uri address = ContentUris.withAppendedId(Uri.parse("content://moviebuff.provider.movies/elements"), id);
        String where = null;
        String whereArgs[] = null;
        String order = null;

        Cursor cursor = contentResolver.query(address,columns, where, whereArgs, order);

        if (cursor != null) {
            cursor.moveToFirst();
            int idPos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_ID);
            int internalId = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_INTERNAL_ID);
            int titlePos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_TITLE);
            int genrePos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_GENRE);
            int homepagePos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_HOMEPAGE);
            int posterPos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_POSTERPATH);
            int overviewPos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_OVERVIEW);
            int releasePos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.MOVIE_RELEASEDATE);

            movie = new Movie(cursor.getInt(idPos), cursor.getInt(internalId), cursor.getString(titlePos), cursor.getString(genrePos), cursor.getString(releasePos), cursor.getString(homepagePos), cursor.getString(overviewPos), cursor.getString(posterPos));
        }

        cursor.close();
        return movie;

    }

    @Override
    public Cursor getCastCursor(Context context, int id) {

        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        String[] columns = new String[] {
                MovieDBOpenHelper.CAST_NAME,
                MovieDBOpenHelper.CAST_ID
        };

        Uri address = ContentUris.withAppendedId(Uri.parse("content://moviebuff.provider.cast/elements"), id);
        String where = null;
        String whereArgs[] = null;
        String order = null;

        Cursor cursor = contentResolver.query(address, columns, where, whereArgs, order);
        return cursor;

    }

    @Override
    public Cursor getSimilarCursor(Context context, int id) {

        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        String[] columns = new String[] {
                MovieDBOpenHelper.SMOVIES_ID,
                MovieDBOpenHelper.SMOVIE_TITLE
        };

        Uri address = ContentUris.withAppendedId(Uri.parse("content://moviebuff.provider.similar/elements"), id);
        String where = null;
        String whereArgs[] = null;
        String order = null;

        Cursor cursor = contentResolver.query(address, columns, where, whereArgs, order);
        return cursor;

    }
}
