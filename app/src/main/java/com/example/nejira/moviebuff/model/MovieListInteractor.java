package com.example.nejira.moviebuff.model;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import com.example.nejira.moviebuff.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MovieListInteractor extends IntentService implements iMovieListInteractor {

    final public static int STATUS_RUNNING = 0;
    final public static int STATUS_FINISHED = 1;
    final public static int STATUS_ERROR = 2;

    ArrayList<Movie> movies;
    private String tmdb_api_key = BuildConfig.API_KEY;
    private MovieDBOpenHelper movieDBOpenHelper;
    SQLiteDatabase database;
    private Movie movie;

    public MovieListInteractor() {
        super(null);
    }

    public MovieListInteractor(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        // DOHVATANJE TRAZENOG FILMA SA API-A

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);
        String params = intent.getStringExtra("query");
        String query = null;

        try {
            query = URLEncoder.encode(params, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url1 = "https://api.themoviedb.org/3/search/movie?api_key=" + tmdb_api_key + "&query=" + query;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);
            JSONArray results = jo.getJSONArray("results");
            movies = new ArrayList<>();

            for (int i = 0; i < results.length(); i++){
                JSONObject m = results.getJSONObject(i);
                Integer id = m.getInt("id");
                String title = m.getString("title");
                String releaseDate = "";
                if (m.has("release_date")) {
                    releaseDate = m.getString("release_date");
                }
                String overview = m.getString("overview");
                String posterPath = m.getString("poster_path");
                movies.add(new Movie(id, title, releaseDate, overview, posterPath));
                if (i == 4) break;
            }

        } catch (ClassCastException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (MalformedURLException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (IOException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (JSONException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        }

        bundle.putParcelableArrayList("result", movies);
        receiver.send(STATUS_FINISHED, bundle);

    }

    public void Details() {

        // DOHVATANJE ZVIJEZDA FILMA IZ TABELE ZA ODGOVARAJUCI FILM

        String query = "SELECT " + MovieDBOpenHelper.CAST_NAME + " FROM " +
                MovieDBOpenHelper.CAST_TABLE + " WHERE "+
                MovieDBOpenHelper.CAST_MOVIE_ID + " = ?";

        Cursor cursor = database.rawQuery(query, new String[]{Integer.toString(movie.getId())});
        ArrayList<String> cast = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                int pos = cursor.getColumnIndexOrThrow(MovieDBOpenHelper.CAST_NAME);
                cast.add(cursor.getString(pos));
            } while (cursor.moveToNext());
        }

        movie.setCast(cast);
        cursor.close();

        // DOHVATANJE SLICNIH FILMOVA KAO STO JE ODGOVARAJUCI FILM

        query = "SELECT " + MovieDBOpenHelper.SMOVIE_TITLE + " FROM " +
                MovieDBOpenHelper.SIMILIAR_MOVIES + " WHERE " +
                MovieDBOpenHelper.SMOVIES_MOVIE_ID + " =?";

        Cursor cursor1 = database.rawQuery(query, new String[]{Integer.toString(movie.getId())});
        ArrayList<String> similarMovies = new ArrayList<>();

        if (cursor1.moveToFirst()) {
            do {
                int pos = cursor1.getColumnIndexOrThrow(MovieDBOpenHelper.SMOVIE_TITLE);
                similarMovies.add(cursor1.getString(pos));
            } while (cursor1.moveToNext());
        }

        movie.setSimilarMovies(similarMovies);
        cursor1.close();

    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    public Cursor getMovieCursor(Context context) {

        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        String[] columns = new String[] {MovieDBOpenHelper.MOVIE_INTERNAL_ID, MovieDBOpenHelper.MOVIE_TITLE, MovieDBOpenHelper.MOVIE_RELEASEDATE, MovieDBOpenHelper.MOVIE_POSTERPATH};
        Uri address = Uri.parse("content://moviebuff.provider.movies/elements");

        String where = null;
        String whereArgs[] = null;
        String order = null;

        Cursor cursor = contentResolver.query(address, columns, where, whereArgs, order);
        return cursor;

    }
}
