package com.example.nejira.moviebuff.model;

import android.content.Context;
import android.database.Cursor;

public interface iMovieDetailInteractor {

    void save(Movie movie, Context context);
    Movie getMovie(Context context, Integer id);
    Cursor getCastCursor(Context context, int id);
    Cursor getSimilarCursor(Context context, int id);

}
