package com.example.nejira.moviebuff.model;

import android.content.Context;
import android.database.Cursor;

public interface iMovieListInteractor {

    Cursor getMovieCursor(Context context);

}
