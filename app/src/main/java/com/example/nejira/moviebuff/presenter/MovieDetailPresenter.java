package com.example.nejira.moviebuff.presenter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;

import com.example.nejira.moviebuff.model.Movie;
import com.example.nejira.moviebuff.model.MovieDetailInteractor;
import com.example.nejira.moviebuff.model.iMovieDetailInteractor;
import com.example.nejira.moviebuff.receivers.MovieDetailResultReceiver;
import com.example.nejira.moviebuff.view.iMovieDetailView;

public class MovieDetailPresenter implements iMovieDetailPresenter, MovieDetailResultReceiver.Receiver {

    private Context context;
    private iMovieDetailView view;
    private Movie movie;
    private iMovieDetailInteractor movieDetailInteractor;
    private MovieDetailResultReceiver movieDetailResultReceiver;

    public MovieDetailPresenter(iMovieDetailView view, Context context) {
        this.view = view;
        this.context = context;
        this.movieDetailInteractor = new MovieDetailInteractor();
    }

    public MovieDetailPresenter(Context context) {
        this.context = context;
        this.movieDetailInteractor = new MovieDetailInteractor();
    }

    @Override
    public void setMovie(Parcelable movie) {
        this.movie = (Movie) movie;
    }

    @Override
    public Movie getMovie() {
        return movie;
    }

    @Override
    public void searchMovie(String query) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, context, MovieDetailInteractor.class);
        intent.putExtra("query", query);
        movieDetailResultReceiver = new MovieDetailResultReceiver(new Handler());
        movieDetailResultReceiver.setReceiver(MovieDetailPresenter.this);
        intent.putExtra("receiver", movieDetailResultReceiver);
        context.getApplicationContext().startService(intent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case MovieDetailInteractor.STATUS_RUNNING:
                view.showToast("Searching");
                break;
            case MovieDetailInteractor.STATUS_FINISHED:
                Movie result = resultData.getParcelable("result");
                view.showToast("Successful");
                movie = result;
                movieDetailInteractor.save(result, context.getApplicationContext());
                view.refreshView();
                break;
            case MovieDetailInteractor.STATUS_ERROR:
                view.showToast("Error");
                break;
        }
    }

    @Override
    public void getDatabaseMovie(int id) {
        movie = movieDetailInteractor.getMovie(context, id);
    }

    @Override
    public Cursor getCastCursor(int id) {
        return movieDetailInteractor.getCastCursor(context, id);
    }

    @Override
    public Cursor getSimilarCursor(int id) {
        return movieDetailInteractor.getSimilarCursor(context, id);
    }
}
