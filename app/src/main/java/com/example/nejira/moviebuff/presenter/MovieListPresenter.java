package com.example.nejira.moviebuff.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.nejira.moviebuff.model.Movie;
import com.example.nejira.moviebuff.model.MovieListInteractor;
import com.example.nejira.moviebuff.model.iMovieListInteractor;
import com.example.nejira.moviebuff.receivers.MovieListResultReceiver;
import com.example.nejira.moviebuff.view.iMovieListView;

import java.util.ArrayList;

public class MovieListPresenter implements iMovieListPresenter, MovieListResultReceiver.Receiver {


    private iMovieListView view;
    private Context context;
    private MovieListResultReceiver movieListResultReceiver;
    private iMovieListInteractor movieListInteractor;

    public MovieListPresenter(iMovieListView view, Context context) {
        this.view = view;
        this.context = context;
        this.movieListInteractor = new MovieListInteractor();
    }

    @Override
    public void searchMovies(String query) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, context, MovieListInteractor.class);
        intent.putExtra("query", query);
        movieListResultReceiver = new MovieListResultReceiver(new Handler());
        movieListResultReceiver.setReceiver(MovieListPresenter.this);
        intent.putExtra("receiver", movieListResultReceiver);
        context.getApplicationContext().startService(intent);
    }

    @Override
    public void getMoviesCursor() {
        view.setCursor(movieListInteractor.getMovieCursor(context.getApplicationContext()));
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case MovieListInteractor.STATUS_RUNNING:
                view.showToast("Searching");
                break;
            case MovieListInteractor.STATUS_FINISHED:
                ArrayList<Movie> results = resultData.getParcelableArrayList("result");
                view.showToast("Successful");
                view.setMovies(results);
                break;
            case MovieListInteractor.STATUS_ERROR:
                view.showToast("Error");
                break;
        }
    }
}
