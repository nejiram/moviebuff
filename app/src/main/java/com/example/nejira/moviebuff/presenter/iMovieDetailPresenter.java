package com.example.nejira.moviebuff.presenter;

import android.database.Cursor;
import android.os.Parcelable;

import com.example.nejira.moviebuff.model.Movie;

public interface iMovieDetailPresenter {

    void setMovie(Parcelable movie);
    Movie getMovie();
    void searchMovie(String query);
    void getDatabaseMovie(int id);
    Cursor getCastCursor(int id);
    Cursor getSimilarCursor(int id);

}
