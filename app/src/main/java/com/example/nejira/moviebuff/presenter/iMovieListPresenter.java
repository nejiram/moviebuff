package com.example.nejira.moviebuff.presenter;

public interface iMovieListPresenter {

    void searchMovies(String query);
    void getMoviesCursor();

}
