package com.example.nejira.moviebuff.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (!wifi.isAvailable() || !mobile.isAvailable()) {
            Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();
        }

    }
}
