package com.example.nejira.moviebuff.view;

import android.app.Fragment;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.receivers.MyBroadcastReceiver;
import com.facebook.stetho.Stetho;

public class Main extends AppCompatActivity implements MovieListFragment.OnItemClick {

    private Boolean twoPaneMode;
    private MyBroadcastReceiver receiver = new MyBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        Stetho.initializeWithDefaults(this);

        android.app.FragmentManager fragmentManager = getFragmentManager();
        FrameLayout details = findViewById(R.id.movie_detail);

        // LAYOUT ZA SIROKE EKRANE
        if (details != null) {
            twoPaneMode = true;
            MovieDetailFragment movieDetailFragment = (MovieDetailFragment) fragmentManager.findFragmentById(R.id.movie_detail);

            if (movieDetailFragment == null) {
                movieDetailFragment = new MovieDetailFragment();
                fragmentManager.beginTransaction().replace(R.id.movie_detail, movieDetailFragment).commit();
            }
        } else {
            twoPaneMode = false;
        }

        Fragment movieListFragment = fragmentManager.findFragmentByTag("list");

        if (movieListFragment == null) {
            movieListFragment = new MovieListFragment();
            fragmentManager.beginTransaction().replace(R.id.movie_list, movieListFragment, "list").commit();
        } else {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onItemClicked(Boolean inDatabase, int id) {
        Bundle arguments = new Bundle();
        if (!inDatabase) {
            arguments.putInt("id", id);
        } else {
            arguments.putInt("internal_id", id);
        }

        MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
        movieDetailFragment.setArguments(arguments);

        if (twoPaneMode) {
            getFragmentManager().beginTransaction().replace(R.id.movie_detail, movieDetailFragment).commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.movie_list, movieDetailFragment).addToBackStack(null).commit();
        }

    }
}
