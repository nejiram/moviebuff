package com.example.nejira.moviebuff.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.model.MovieDBOpenHelper;
import com.example.nejira.moviebuff.presenter.MovieDetailPresenter;
import com.example.nejira.moviebuff.presenter.iMovieDetailPresenter;

import java.util.ArrayList;

public class MovieCastFragment extends Fragment {

    private iMovieDetailPresenter presenter;

    public iMovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieDetailPresenter(getActivity());
        }
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.movie_cast_fragment, container, false);
        ListView listView = view.findViewById(R.id.cast_list);

        if (getArguments().containsKey("cast")) {
            ArrayList<String> cast = getArguments().getStringArrayList("cast");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, cast);
            listView.setAdapter(adapter);
        } else {
            SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                    getActivity(),
                    android.R.layout.simple_list_item_1,
                    getPresenter().getCastCursor(getArguments().getInt("id")),
                    new String[] {MovieDBOpenHelper.CAST_NAME},
                    new int[] {android.R.id.text1},
                    SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
            listView.setAdapter(simpleCursorAdapter);
        }

        return view;

    }
}
