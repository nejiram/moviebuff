package com.example.nejira.moviebuff.view;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.model.Movie;
import com.example.nejira.moviebuff.presenter.MovieDetailPresenter;
import com.example.nejira.moviebuff.presenter.iMovieDetailPresenter;

public class MovieDetailFragment extends Fragment implements iMovieDetailView {

    private TextView titleText;
    private TextView genreText;
    private TextView releaseDateText;
    private TextView homepageText;
    private TextView overviewText;
    private ToggleButton toggleButton;
    private ImageView imageView;
    private Button recommendButton;

    private String posterPath = "https://image.tmdb.org/t/p/w342";

    private iMovieDetailPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.movie_detail_fragment, container, false);

        titleText = view.findViewById(R.id.titleText);
        genreText = view.findViewById(R.id.genreText);
        releaseDateText = view.findViewById(R.id.releaseDateText);
        homepageText = view.findViewById(R.id.homepageText);
        overviewText = view.findViewById(R.id.overviewText);
        imageView = view.findViewById(R.id.movie_image);
        recommendButton = view.findViewById(R.id.recommendButton);
        toggleButton = view.findViewById(R.id.stars_cast_button);


        homepageText.setPaintFlags(homepageText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        if (getArguments() != null && getArguments().containsKey("id")) {
            int id = getArguments().getInt("id");
            getPresenter().searchMovie(String.valueOf(id));
        }

        if (getArguments() != null && getArguments().containsKey("internal_id")) {
            int id = getArguments().getInt("internal_id");
            getPresenter().getDatabaseMovie(id);
            refreshView();
        }

        // klikom na homepage tekst otvara se homepage odabranog filma
        homepageText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getPresenter().getMovie().getHomepage();
                Intent url_intent = new Intent(Intent.ACTION_VIEW);
                url_intent.setData(Uri.parse(url));
                if (url_intent.resolveActivity(getActivity().getPackageManager()) != null){
                    startActivity(url_intent);
                }
            }
        });

        // klikom na naziv filma otvara se youtube za pregled trailera
        titleText.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                Intent yt_intent = new Intent(Intent.ACTION_SEARCH);
                yt_intent.setPackage("com.google.android.youtube");
                yt_intent.putExtra("query", getPresenter().getMovie().getTitle() + " trailer");
                yt_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (yt_intent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(yt_intent);
                }
            }
        });

        // klikom na recommend button moguce je poslati overview filma preko aplikacije po izboru
        recommendButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                Intent recommend_intent = new Intent(Intent.ACTION_SEND);
                recommend_intent.putExtra(Intent.EXTRA_TEXT, getPresenter().getMovie().getOverview());
                recommend_intent.setType("text/plain");
                if (recommend_intent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(recommend_intent);
                }

            }
        });

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Bundle arguments = new Bundle();
                    if (getPresenter().getMovie().getInternalId() != null) {
                        arguments.putInt("id", getPresenter().getMovie().getId());
                    } else {
                        arguments.putStringArrayList("similar", getPresenter().getMovie().getSimilarMovies());
                    }
                    SimilarMoviesFragment similarMoviesFragment = new SimilarMoviesFragment();
                    similarMoviesFragment.setArguments(arguments);
                    getChildFragmentManager().beginTransaction().replace(R.id.frameLayout, similarMoviesFragment).commit();
                } else {
                    Bundle arguments = new Bundle();
                    if (getPresenter().getMovie().getInternalId() != null) {
                        arguments.putInt("id", getPresenter().getMovie().getId());
                    } else {
                        arguments.putStringArrayList("cast", getPresenter().getMovie().getCast());
                    }
                    MovieCastFragment movieCastFragment = new MovieCastFragment();
                    movieCastFragment.setArguments(arguments);
                    getChildFragmentManager().beginTransaction().replace(R.id.frameLayout, movieCastFragment).commit();
                }
            }
        });

        return view;
    }

    @Override
    public void refreshView() {
        Movie movie = getPresenter().getMovie();
        titleText.setText(movie.getTitle());
        genreText.setText(movie.getGenre());
        overviewText.setText(movie.getOverview());
        releaseDateText.setText(movie.getReleaseDate());
        homepageText.setText(movie.getHomepage());
        Glide.with(getContext())
                .load(posterPath + movie.getPosterPath())
                .centerCrop()
                .placeholder(R.drawable.movie_icon)
                .error(R.drawable.movie_icon)
                .fallback(R.drawable.movie_icon)
                .into(imageView);

        Bundle arguments = new Bundle();
        if (movie.getInternalId() != null) {
            arguments.putInt("id", movie.getId());
        } else {
            arguments.putStringArrayList("cast", movie.getCast());
        }
        MovieCastFragment movieCastFragment = new MovieCastFragment();
        movieCastFragment.setArguments(arguments);
        getChildFragmentManager().beginTransaction().add(R.id.frameLayout, movieCastFragment).commit();

    }

    @Override
    public void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    public iMovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieDetailPresenter(this, getActivity());
        }
        return  presenter;
    }
}
