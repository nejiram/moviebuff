package com.example.nejira.moviebuff.view;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.adapters.MovieListAdapter;
import com.example.nejira.moviebuff.adapters.MovieListCursorAdapter;
import com.example.nejira.moviebuff.model.Movie;
import com.example.nejira.moviebuff.model.MovieDBOpenHelper;
import com.example.nejira.moviebuff.presenter.MovieListPresenter;
import com.example.nejira.moviebuff.presenter.iMovieListPresenter;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class MovieListFragment extends Fragment implements iMovieListView {

    private Button button;
    private EditText editText;
    private ListView listView;

    private MovieListAdapter movieListAdapter;
    private MovieListCursorAdapter movieListCursorAdapter;
    private MovieListPresenter movieListPresenter;

    private OnItemClick onItemClick;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.movie_list_fragment, container, false);
        movieListAdapter = new MovieListAdapter((Main) getActivity(), R.layout.movie_list_element, new ArrayList<Movie>());
        movieListCursorAdapter = new MovieListCursorAdapter(getActivity(), R.layout.movie_list_element, null, false);

        button = view.findViewById(R.id.button);
        editText = view.findViewById(R.id.editText);
        listView = view.findViewById(R.id.listView);

        listView.setAdapter(movieListCursorAdapter);

        listView.setOnItemClickListener(listCursorItemClickListener);

        onItemClick = (OnItemClick) getActivity();

        getPresenter().getMoviesCursor();

        // uzimamo intent koji je pozvao aktivnost
        Intent intent = getActivity().getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        // podatke upisujemo u textview
        if (intent.ACTION_SEND.equals(action) && type != null) {
            if (type.equals("text/plain")){
                handleSendText(intent);
            }
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movieListPresenter.searchMovies(editText.getText().toString());
            }
        });

        return view;
    }

    private void handleSendText(Intent intent){
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            editText.setText(sharedText);
        }
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Movie movie = movieListAdapter.getMovie(position);
            onItemClick.onItemClicked(false,movie.getId());
        }
    };

    private AdapterView.OnItemClickListener listCursorItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor cursor = (Cursor) parent.getItemAtPosition(position);
            Log.d("KAMIJON", "CLICKED");
            if (cursor != null) {
                onItemClick.onItemClicked(true, cursor.getInt(cursor.getColumnIndex(MovieDBOpenHelper.MOVIE_INTERNAL_ID)));
            }
        }
    };

    public void setMovies(ArrayList<Movie> movies) {
        listView.setAdapter(movieListAdapter);
        listView.setOnItemClickListener(listItemClickListener);
        movieListAdapter.setMovies(movies);
    }

    @Override
    public void notifyMovieListDataSetChanged() {
        movieListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCursor(Cursor cursor) {
        listView.setAdapter(movieListCursorAdapter);
        listView.setOnItemClickListener(listCursorItemClickListener);
        movieListCursorAdapter.changeCursor(cursor);
    }

    public iMovieListPresenter getPresenter() {
        if (movieListPresenter == null) {
            movieListPresenter = new MovieListPresenter((iMovieListView) this, getActivity());
        }
        return movieListPresenter;
    }

    public interface OnItemClick {
        void onItemClicked(Boolean inDatabase, int id);
    }

}
