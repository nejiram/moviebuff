package com.example.nejira.moviebuff.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.nejira.moviebuff.R;
import com.example.nejira.moviebuff.model.MovieDBOpenHelper;
import com.example.nejira.moviebuff.presenter.MovieDetailPresenter;
import com.example.nejira.moviebuff.presenter.iMovieDetailPresenter;

import java.util.ArrayList;

public class SimilarMoviesFragment extends Fragment {

    private iMovieDetailPresenter presenter;

    public iMovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieDetailPresenter(getActivity());
        }
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.similar_movies_fragment, container, false);
        ListView listView = view.findViewById(R.id.similar_movies);

        if (getArguments().containsKey("similar")) {
            ArrayList<String> similar = getArguments().getStringArrayList("similar");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, similar);
            listView.setAdapter(adapter);
        } else {
            SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                    getActivity(),
                    android.R.layout.simple_list_item_1,
                    getPresenter().getSimilarCursor(getArguments().getInt("id")),
                    new String[] {MovieDBOpenHelper.SMOVIE_TITLE},
                    new int[] {android.R.id.text1},
                    SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
            listView.setAdapter(simpleCursorAdapter);
        }
        return view;

    }

}
