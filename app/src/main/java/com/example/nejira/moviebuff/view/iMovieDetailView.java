package com.example.nejira.moviebuff.view;

public interface iMovieDetailView {

    void refreshView();
    void showToast(String text);

}
