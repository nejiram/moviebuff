package com.example.nejira.moviebuff.view;

import android.database.Cursor;

import com.example.nejira.moviebuff.model.Movie;

import java.util.ArrayList;

public interface iMovieListView {

    void setMovies(ArrayList<Movie> movies);
    void notifyMovieListDataSetChanged();
    void showToast(String text);
    void setCursor(Cursor cursor);

}
